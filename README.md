# Madrasa schools and the skills they impart



The most well-known of these schools is known as a madrasa. As a rule, madrasas center around showing the Qur'an, the recorded platitudes of the Prophet Muhammad, consecrated regulation and other Islamic subjects. Learn About: [**online madrasa**](https://onlinemadrasa.org/online-quran-classes-for-kids/)
The inquiry is: How well do these schools plan understudies for occupations in economies in view of contemporary information and innovation?

**History of the madrasa**
Madrasas have a long and rich history. After the introduction of Islam in the seventh 100 years, Muslims who needed a strict schooling joined concentrate on circles in mosques where educators gave guidance.
Over the course of the following 400 years, extra focuses of learning, established and enriched by rulers, high authorities and well off individuals from the local area, met out in the open and confidential libraries. These were early types of madrasas.
By the eleventh hundred years, madrasas were deep rooted free focuses of learning with a portion of the highlights they hold today. They had extremely durable structures, paid staff and occupant researchers with living quarters and payments. Understudies were given food and lodging, and got free instruction.
For what reason do guardians pick madrasas?
Today, madrasas are most prevailing in Pakistan - 20,000 are enlisted and thousands more are unregistered - yet the quantity of madrasas is filling in many regions of the planet.
In Egypt, they expanded from 1,855 to 4,314 somewhere in the range of 1986 and 1996. In Mali, one out of four elementary younger students went to madrasas during 2005. In India, where 14 individuals out of 100 are Muslim, the public authority reports that, generally, 4% of Muslim understudies go to madrasas full-time.

During 2013, a concentrate by researchers of a provincial region in northwestern India shows why a few guardians pick madrasa training for their kids. In this ruined locale, called Mewat, which is larger part Muslim, almost 10% of Muslim understudies - over two times the public normal - are signed up for madrasas.
Most alumni from Mewat's madrasas secure inadequately paid positions in madrasas, mosques or hallowed places of holy people, a couple of others in cultivating. Just 3% accomplish a more significant level of financial turn of events.
Most of Mewat's Muslim families believe that madrasas should offer specialized courses and professional preparation. Notwithstanding, the investigation's creators discovered that the strict pioneers who could endorse changes are "set against the advanced schooling."

**Turkey's shift to strict schools**

To get back to Turkey, in fact talking, this nation dispensed with madrasas almost a long time back. In 1924, the principal president, Mustafa Kemal Atatürk, supplanted madrasas with nonreligious schools in view of a Western instructive model or changed over them into schools called Imam Hatip for the preparation of Muslim evangelists. Learn About: [**quran for kids beginners**](https://onlinemadrasa.org/online-quran-classes-for-kids/)
Nonetheless, when Erdogan expected power in 2002, he and his Justice and Development party transformed Imam Hatip schools into strict center and secondary schools whose graduates could apply into any college program. Somewhere in the range of 2003 and 2012, enlistment in Imam Hatip schools significantly increased. Quite a while back, there were 450 such schools. Today there are 4,500.
Erdogan's administration carried out additional instructive changes in 2017. Imam Hatip schools presently expect understudies to concentrate on the idea of blessed battle, to discover that Muslims shouldn't wed skeptics, and to accept that spouses ought to submit to husbands. These schools additionally underline repetition learning over decisive reasoning.
At the point when Erdogan has vowed to support Turkey from the seventeenth world economy to the best 10, the methodology that he has decided to "raise a devout age" may wreck this desire.
